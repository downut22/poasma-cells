﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cellule_Part : MonoBehaviour
{
    public string celluleName = "Cellule";

    public float distance;
    public float orientation;
    public float angleRotation;
    public float vitesseRotation;

    public Vector3 position;

    public Vector2 vitesse;

    public Vector2 vitesseRelative;

    public Cellule noyau;

    public bool killed;

    int sensRotation = 1;

    int destructionDelay;

    public SpriteRenderer visual;

    void Start()//Cette fonction intégrée à Unity est appelée à l'apparition de ce morceau de cellule
    {
        if (GetComponent<Cellule>() == null)
        {
            if (noyau == null)
            {
                //Récupère le centre de la cellule
                noyau = GameObject.Find(celluleName).GetComponent<Cellule>();
            }
            //Définit l'orientation de cette partie de la cellule
            orientation = ( ( (orientation - 50) / 50f ) * 90);
            transform.localRotation = Quaternion.Euler(0, 0, orientation);

            //Définit la distance de cette partie par rapport à son parent
            distance = (distance * 0.0025f);
            transform.position = transform.parent.position + (transform.up * distance * noyau.transform.localScale.magnitude);

            //Définit la vitesse rotation de cette partie de la cellule, entre 5 et -5
            vitesseRotation = (vitesseRotation - 50);

            gameObject.layer = 9;
        }
        else
        {
            noyau = GetComponent<Cellule>();
            gameObject.layer = 8;
        }
        position = transform.position;
    } 

    void FixedUpdate()//Cette fonction intégrée à Unity est appelée une fois par frame
    {
        if (!killed && noyau != null)
        {
            Information();

            Rotation();

            Fluide();

            Destruction();
        }
     
        if (GetComponent<Collider2D>() != null && GetComponent<Cellule>()==null)
        {
            GetComponent<Collider2D>().enabled = noyau.borned;
        }
        if (noyau.borned && gameObject.layer != 9)
        {
            gameObject.layer = 9;
        }      

        Coloration();

        destructionDelay--;
    }

    void Information() //Récupère des informations sur cette partie de la cellule
    {
        //Calcul de la vitesse de cette partie de la cellule
        vitesse = this.transform.position - position;

        //Calcul de la vitesse relative au noyau de la cellule
        vitesseRelative = vitesse - noyau.GetComponent<Cellule_Part>().vitesse;

        //Obtention de la position actuelle de cette partie de la cellule
        position = this.transform.position;
    }

    void Rotation() //Fait tourner cette partie de la cellule avec une certaine vitesse
    {
        //Test de l'angle actuel de cette partie de cellule
        if (Mathf.Abs(Quaternion.Angle(transform.localRotation, Quaternion.Euler(0, 0, orientation))) >= angleRotation)
        {
            sensRotation = -sensRotation; //On inverse le sens de rotation si l'angle dépasse l'angle maximal
        }
        transform.Rotate(new Vector3(0, 0, sensRotation), vitesseRotation * noyau.manager.vitesseRotation); //On fait tourner cette partie dans un certain sens avec une certain vitesse
    }

    void Fluide() //Application d'une force à la cellule en fonction de la vitesse relative au centre de la celllule, comme si elle se déplaçait dans un fluide 
    {
        Vector2 distanceCentre = noyau.transform.position - transform.position; //Récupération du vecteur de cette partie jusqu'au centre de la cellule 
        float force = (Vector2.Dot(distanceCentre.normalized, vitesseRelative.normalized) + 0.5f) * vitesseRelative.magnitude; //Définit l'intensité de la force qui poussera la cellule, en fonction de la vitesse relative de cette partie à son centre
        noyau.GetComponent<Rigidbody2D>().AddForce(distanceCentre.normalized * force * noyau.manager.viscosity * noyau.manager.vitesse,ForceMode2D.Force); //Application de la force
    }

    void Destruction() //En cas de collision avec un autre morceau de cellule, détruit ce morceau ou le colisionné, en fonction de la vitesse d'impact
    {
        if(destructionDelay > 0){return ;}
        destructionDelay = 30;

        Collider2D victime = Physics2D.OverlapCircle(this.transform.position, this.transform.lossyScale.magnitude * noyau.manager.taille);
                if (victime != null && victime.TryGetComponent(out Cellule_Part part))
                {
                    if (part.noyau != null && noyau != null && !part.killed && part.noyau.borned && noyau.borned)
                    {
                        if (part.noyau.celluleName != noyau.celluleName)
                        {
                            //print(a.GetComponent<Cellule_Part>().celluleName);
                            Vector2 differenceVitesse = part.vitesse - vitesse;
                            if (differenceVitesse.magnitude > noyau.manager.vitesseDestruction)
                            {
                                if (part.vitesse.magnitude > vitesse.magnitude)
                                {
                                    Deces();
                                }
                                else
                                {
                                    part.Deces();
                                }
                            }
                        }
                    }
        }
    }

    void Deces() //Si ce morceau de cellule à été détruit, on le décroche du reste de la cellule, et on détruit les autre morceaux qui y sont accrochés. Si le morceau a suffisament réduit de taille (en étant mangé), on le fait disparaitre
    {
        if (!killed)
        {
            int i = 0;
            while (i < transform.childCount)
            {
                if (transform.GetChild(i).TryGetComponent(out Cellule_Part part))
                {
                    part.Deces();
                }
                i++;
            }

            transform.parent = null;
        }
        killed = true;
        /*
        if(this.transform.localScale.magnitude < noyau.manager.dispawnTaille)
        {
            Destroy(gameObject);
        }*/
    }

    void Coloration() //Définit la couleur de ce morceau de cellule en fonction des circonstances (mort ou pas mort)
    {
        if (killed)
        {
            visual.color = Color.red;
        }
        else
        {
            visual.color = noyau.couleur;
            /*
            if (GetComponent<Cellule>() != null)
            {
                visual.color = Color.yellow;
            }
            */
        }
    }

}
