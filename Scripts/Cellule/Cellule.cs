﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cellule : MonoBehaviour
{
    [Header("Caractérisitques")]

    public string celluleName = "Cellule";

    public string gene = "";

    public Color couleur;

    [Header("In game")]

    public float nutrition;

    public List<GameObject> decendantes;

    public bool borned = true;

    [Header("Others")]

    public Manager manager;

    List<char> alphabet = new List<char>(26) { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

    List<char> chiffres = new List<char>(10) { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

    public float massParties = 0.05f;

    public GameObject cellPart;

    public GameObject cellulePrefab;

    int nam;

    private void Awake() // Cette fonction intégrée à Unity est appelée dès que cette cellule apparait
    {
        Initialisation(true);
        transform.localScale = Vector3.one * manager.size;
    }

    void Initialisation(bool random) //Cette fonction initialise la cellule. Le parametre permet de choisir si on doit générer un nouveau gène aléatoire ou non.
    {
        nam = Random.Range(0, 100000);
        manager = GameObject.Find("Manager").GetComponent<Manager>();
        if (random)
        {
            gene = GenerateGene();
        }
        ReadGene();
        celluleName = gene + nam.ToString();
        gameObject.name = celluleName;
        this.GetComponent<Cellule_Part>().celluleName = celluleName + nam.ToString();
        float longueurGene = gene.Length / 9f;
        GetComponent<Rigidbody2D>().mass = massParties * longueurGene;
    }

    void ReadGene()
    {
        Transform[] parents = new Transform[26]; //Crée une liste de 26 parents (aux quels pourront s'accrocher les différents morceaux de cellules
        parents[0] = this.GetComponent<Transform>(); //Définit le parent 0 en tant que le noyau de la cellule (donc cet objet ci)

        int i = 0;
        while (i < gene.Length) //Boucle qui parcours le gene du début à sa fin
        {
            if (alphabet.Contains(gene[i])) //Si le caractère lu est une lettre (est contenu dans l'alphabet), on continue
            {
                int positionalphabet = alphabet.IndexOf(gene[i]); // On récupère la position de cette lettre dans l'alphabet

                while (parents[positionalphabet] == null) //Si le parent recherché (associé à la lettre actuelle) n'existe pas 
                    //(encore), on prend le précédent, jusqu'à tomber sur un parent existant
                {
                    positionalphabet = positionalphabet - 1;
                }

                // Supprime tout les parents de la liste de parents aprés le parent sélectionné pour cette lettre. Ainsi, par exemple,
                //dans un gene "A B C A B D", le dernier D s'accrochera au B précédent, plutôt qu'au C, même si le C est plus proche dans l'alphabet
                int x = positionalphabet + 1;
                while (x < parents.Length)
                {
                    parents[x] = null;
                    x = x + 1;
                }

                // Création d'un morceau de cellule (cellPart) nommé 'fille' et on le fait apparaitre (Instantiate), accroché 
                //au parent de qui a pour position dans la liste la position de la lettre actuelle dans alphabet (    parents[positionalphabet])    )
                GameObject fille = Instantiate(cellPart, parents[positionalphabet]);

                //Définit le parent de la lettre suivante en tant que le morceau (fille) que l'on vient de créer
                parents[positionalphabet + 1] = fille.transform;

                while (alphabet.Contains(gene[i]) && i < gene.Length) //On avance jusqu'au prochain chiffre ( càd tant que le caractère lu est une lettre)
                {
                    i = i + 1;
                }

                //Lit les chiffres qui suivent
                List<char> caracteres = new List<char>();     // crée un liste de char (lettre ou chiffre seul, par ex : 'a' ou '1') qui contiendra 
                                                                //les chiffres associés à la lettre actuelle     
                bool b = false;
                while (b == false) 
                {
                    caracteres.Add(gene[i]); //on ajoute l'élément actuel (ici les chiffres après la lettre actuelle) à la liste de char

                    i = i + 1;

                    if (i >= gene.Length) //on s'arrete si on dépasse la longueur du gene
                    {
                        b = true;
                    }
                    else if (alphabet.Contains(gene[i])) // on s'arrete si l'élement actuel est une lettre
                    {
                        b = true;
                    }
                }

                i = i - 1;//On repasse à l'élément précédent (puisqu'on s'arrete sur une lettre avec la boucle précédente)

                //Si la liste contenant les chiffres en contient moins que 8, on ajoute des '0' pour qu'elle ait la bonne taille
                while (caracteres.Count < 8) 
                {
                    caracteres.Add('0');
                }

                //On associe chaque paire de chiffre aux caractéristiques correspondantes du morceau de cellule que l'on a créé (fille)
                fille.GetComponent<Cellule_Part>().distance = int.Parse(caracteres[0].ToString() + caracteres[1].ToString());
                fille.GetComponent<Cellule_Part>().orientation = int.Parse(caracteres[2].ToString() + caracteres[3].ToString());
                fille.GetComponent<Cellule_Part>().angleRotation = int.Parse(caracteres[4].ToString() + caracteres[5].ToString());
                fille.GetComponent<Cellule_Part>().vitesseRotation = int.Parse(caracteres[6].ToString() + caracteres[7].ToString());

                fille.GetComponent<Cellule_Part>().noyau = GetComponent<Cellule>();
            }

            i = i + 1;//On repasse à l'élément suivant (donc une lettre)
        }
    }  //Lit le gene actuel de la cellule (contenu dans la variable gene), et génère la cellule à partir de celui ci

    string GenerateGene()//Génère un gène aléatoire, retournée par une chaine de caractère (string)
    {

        string genetic = "";

        int longueur = Random.Range(manager.longueurmini, manager.longueurmaxi);
        int i = 0;
        while (i < longueur)
        {
            string lettre = alphabet[Random.Range(0, 25)].ToString();
            genetic = genetic + lettre;

            int u = 0;
            while (u < 8)
            {
                string chiffresrng = chiffres[Random.Range(0,9)].ToString();
                genetic = genetic + chiffresrng;
                u = u + 1;
            }

            i = i + 1;
        }
        return genetic;
    } 

    void Reproduction() //Fait apparaitre une nouvelle cellule, avec le même gène que celle-ci, mais avec quelques mutations
    {
        manager.actCellCount++;
        GameObject decendant = Instantiate(cellulePrefab, transform.position, transform.rotation);
        decendant.transform.localScale = Vector3.one * manager.spawnTaille;
        decendant.GetComponent<Cellule>().gene = Mutation(gene);
        decendant.GetComponent<Cellule>().borned = false;
        decendant.GetComponent<Cellule>().nutrition = 0;
        decendant.GetComponent<Cellule>().Initialisation(false);
        decendantes.Add(decendant);
        manager.duplicationCounter = manager.duplicationCounter + 1;
    }

    string Mutation(string gene)//Fait muter le gène donné (en paramètre) et le retourne sous forme de chaine de caractère
    {
        string newGene = gene;

        int mutations = Random.Range(1, manager.maxmutations);
        while(mutations > 0)
        {
            int position = Random.Range(0, newGene.Length - 1);

            List<char> car = new List<char>();
            car.AddRange(alphabet);
            car.AddRange(chiffres);
            car.AddRange(chiffres);

            List<char> temp = new List<char>();
            temp.AddRange(newGene.ToCharArray());
            temp[position] = car[Random.Range(0, car.Count - 1)];
            temp.Insert(Random.Range(0, temp.Count - 1), car[Random.Range(0, car.Count - 1)]);
            newGene = new string(temp.ToArray());

            mutations = mutations - 1;
        }
        return newGene;
    } 

    void Naissance() //Cette fonction est appelée en boucle et fait grossir une cellule venant de naitre jusqu'à sa taille adulte. (voir fonction Update() )
    {
        transform.localScale = transform.localScale * (1+manager.spawnVitesse) ;
        GetComponent<Rigidbody2D>().AddForce(transform.up * manager.spawnFuite, ForceMode2D.Force);
    }

    void FixedUpdate() //Cette fonction intégrée à Unity est appelée une fois par frame
    {
        if (!GetComponent<Cellule_Part>().killed)
        {
            this.GetComponent<Rigidbody2D>().drag = manager.viscosity * 0.1f;

            celluleName = gene + nam.ToString();

            if (nutrition >= manager.repus)
            {
                if(manager.actCellCount < manager.cellCount){
                    Reproduction();
                    nutrition = 0;//-nutrition * 2f;
                }
            }
            if (nutrition < 0)
            {
                nutrition++;
            }

            if (!borned)
            {
                Naissance();
                if (transform.lossyScale.magnitude >= Vector3.one.magnitude * manager.size)
                {
                    borned = true;
                }
            }

            if (this.GetComponent<Rigidbody2D>().velocity.magnitude > manager.maxVitesse)
            {
                this.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity.normalized * manager.maxVitesse;
            }
        }
    }

    void Absorption(Cellule_Part victime) //Mange le morceau de cellule mort (en parametre), réduisant sa taille, et augmentant la variable nutrition
    {
        //nutrition = nutrition + ( (victime.transform.localScale.magnitude * manager.vitesseNutrition) / (float)manager.size);
        //victime.transform.localScale = victime.transform.localScale / (float)(1+manager.vitesseNutrition);

        nutrition += manager.vitesseNutrition;
        Destroy(victime.gameObject);

        //victime.GetComponent<Rigidbody2D>().AddForce((this.transform.position - victime.transform.position) * manager.forceNutrition * Time.deltaTime, ForceMode2D.Force);
    }

    void OnDestroy()
    {
        manager.actCellCount--;
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent(out Cellule_Part part))
        {
            if (part.killed)
            {
                Absorption(part);
            }
        }
    } //Cette fonction intégrée à Unity est appelée en boucle lorsqu'une collision à lieu avec cette cellule
}
