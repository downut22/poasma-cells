﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public int nombre = 5;
   
    public Vector2 spawnLimits;

    public GameObject cellulePrefab;

    public InputField entrerNombre;

    void Spawn(int quantite,GameObject objet,Vector2 positionsLimites) //Fait apparaitre un certain
    {
        int i = 0;
        while (i < quantite)
        {
            Vector2 position = new Vector2(Random.Range(-positionsLimites.x, positionsLimites.x), Random.Range(-positionsLimites.y, positionsLimites.y));
            GameObject spawned = Instantiate(objet,position,transform.rotation);
            spawned.GetComponent<Cellule>().couleur = new Color(Random.Range(0.111f, 1f), Random.Range(0.111f, 1f), Random.Range(0.111f, 1f));
            i = i + 1;
        }
    }
    void Start()
    {
        nombre = manager.cellCount;//PlayerPrefs.GetInt("Nombre");
        if(nombre < 3)
        {
            nombre = 3;
        }
        /*
        if(nombre > 100)
        {
            nombre = 100;
        }
        */
        entrerNombre.text = nombre.ToString();
        Spawn(nombre,cellulePrefab,spawnLimits);
    }
    private void Update()
    {   
        if (int.TryParse(entrerNombre.text,out int r))
        {
            nombre = int.Parse(entrerNombre.text);
        }
        PlayerPrefs.SetInt("Nombre", nombre);
    }

    public Manager manager;
}
