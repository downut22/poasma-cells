﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public int longueurmini = 10;
    public int longueurmaxi = 169;

    public int cellCount = 14;

    public float viscosity;
    public float vitesse;
    public float maxVitesse = 4;
    public float vitesseRotation = 0.05f;

    public float vitesseNutrition = 1;
    public float repus = 50f;

    public float vitesseDestruction = 5;
    public float taille = 5;

    public float size = 0.5f;
    public int maxmutations = 10;

    public float spawnTaille = 0.1f;
    public float spawnVitesse = 0.1f;
    public float spawnFuite = 2.5f;
    public float dispawnTaille = 0.2f;

    public int duplicationCounter;

    public Text duplicationShow;
    public Text cellShow;
    public Text appetitShow;
    public Slider appetitSlider;

    public int actCellCount;

    private void Start()
    {
        actCellCount=cellCount;
        appetitSlider.value = repus;
    }

    public void Pause() //Mets la simulation en pause si elle ne l'est pas, et la démarre si elle l'est
    {
        if( Time.timeScale > 0)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void Recommencer() //Supprimer toutes les cellules et redémare la simulation de zéro
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Update()
    {
        duplicationShow.text = duplicationCounter.ToString();
        repus = appetitSlider.value;
        appetitShow.text = repus.ToString("F1");
        cellShow.text = actCellCount.ToString();
    }
}
